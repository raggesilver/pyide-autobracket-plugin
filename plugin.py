"""PyIDE Autobracket plugin."""

from gi.repository import GObject, Gdk


class Plugin(GObject.GObject):
    """The plugin class."""

    def __init__(self, *args, **kwargs):
        """Docstring for __init__."""
        GObject.GObject.__init__(self)

        self.chars = {
            'parenleft': ')',
            'bracketleft': ']',
            'braceleft': '}',
            'quotedbl': '"',
            'apostrophe': '\''  # ,
            # 'less': '>' # FIXME disabled until there is a way to check if
            # the language needs this auto bracket
        }

        self.skip_if_next = False
        self.history = {}  # For storing hadSelection

        self.settings = None

        if 'application_window' in kwargs:
            self.application_window = kwargs['application_window']

        if 'settings' in kwargs:
            self.settings = kwargs['settings'].data.get(
                'pyide_autobracket_plugin', None)

    def do_activate(self):
        """Docstring for do_activate."""
        if self.settings:
            self.do_apply_settings()

        self.application_window.editor.connect(
            "editor-created-after", self.do_attach_to_editor)

    def do_apply_settings(self):
        """Apply settings to self."""
        self.skip_if_next = (
            self.settings.get("skip_if_next", False)
        )

    def do_attach_to_editor(self, *args):
        """Docstring for do_attach_to_editor."""
        current_editor = self.application_window.editor.active_editor

        self.history[current_editor.sview] = {"hadSelection": False}

        if current_editor is not None:
            current_editor.sview.connect("event-after", self.do_completion)

    def do_completion(self, sview, event):
        """Docstring for do_completion."""
        sbuff = sview.get_buffer()
        hasSelection = sbuff.props.has_selection

        # Selection iters
        startIter = endIter = None
        # Selection text

        # Capture selection (in case there is one)
        if hasSelection:
            startIter, endIter = sbuff.get_selection_bounds()
            self.history[sview]["selection"] = sbuff.get_text(
                startIter, endIter, False)
            self.history[sview]["selectionIter"] = (startIter, endIter)

        # Masks to ignore
        ignore = Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.MOD1_MASK

        if (event.type != Gdk.EventType.KEY_PRESS or event.state & ignore
                or Gdk.keyval_name(event.key.keyval) not in self.chars):

            self.history[sview]["hadSelection"] = hasSelection

            return

        insert = self.get_insert(sbuff)
        closing = self.chars[Gdk.keyval_name(event.key.keyval)]

        next_char = insert.get_char()

        # TODO check if the language needs auto bracket for < and >

        if not hasSelection and not self.history[sview]["hadSelection"]:
            if next_char == closing and self.skip_if_next:
                insert.forward_char()
                sbuff.place_cursor(insert)
                return
            # Allows one Ctrl+Z to undo everything until we close user action
            sbuff.begin_user_action()
            sbuff.insert(insert, closing)
        else:
            sbuff.begin_user_action()
            text = self.history[sview]["selection"] + closing
            sbuff.insert(insert, text)

        insert.backward_chars(1)
        sbuff.place_cursor(insert)
        sbuff.end_user_action()

    def get_insert(self, sbuff):
        """Docstring for get_insert."""
        mark = sbuff.get_insert()
        return sbuff.get_iter_at_mark(mark)
